﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Helper.Entities
{
    public enum ContactType : byte
    {
        Skype,
        Facebook,
        VK,
        WhatsApp,
        Telegram
    }
}
