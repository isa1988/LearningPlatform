﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Helper.Entities
{
    /// <summary>
    ///     Пол человека
    /// </summary>
    public enum SexOfMan : byte
    {
        /// <summary>
        ///     Мужской
        /// </summary>
        Male,

        /// <summary>
        ///     Женский
        /// </summary>
        Female
    }
}
