﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Helper.Entities
{
    /// <summary>
    /// Уровни языка
    /// </summary>
    public enum LevelOfLanguage : byte
    {
        /// <summary>
        /// A1: Начальный уровень
        /// </summary>
        A1,

        /// <summary>
        /// A2: Элементарный уровень
        /// </summary>
        A2,
        
        /// <summary>
        /// B1: Средний уровень
        /// </summary>
        B1,
        
        /// <summary>
        /// B2: Пороговый продвинутый уровень
        /// </summary>
        B2,
        
        /// <summary>
        /// C1: Продвинутый уровень
        /// </summary>
        C1,

        /// <summary>
        /// C2: Владение в совершенстве
        /// </summary>
        C2
    }
}
