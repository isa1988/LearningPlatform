﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Helper.Entities
{
    /// <summary>
    /// Часовой пояс
    /// </summary>
    public enum Timezone : byte
    {
        UTC,

        UTCB12,
        UTCB11,
        UTCB10,
        UTCB9M30,
        UTCB9,
        UTCB8,
        UTCB7,
        UTCB6,
        UTCB5,
        UTCB4,
        UTCB3M30,
        UTCB3,
        UTCB2,
        UTCB1,
            
        UTCA1,
        UTCA2,
        UTCA3,
        UTCA3M30,
        UTCA4,
        UTCA4M30,
        UTCA5,
        UTCA5M30,
        UTCA5M45,
        UTCA6,
        UTCA6M30,
        UTCA7,
        UTCM8,
        UTCA8M45,
        UTCA9,
        UTCA9M30,
        UTCA10,
        UTCA10M30,
        UTCA11,
        UTCA12,
        UTCA12M45,
        UTCA13,
        UTCA14
    }
}
