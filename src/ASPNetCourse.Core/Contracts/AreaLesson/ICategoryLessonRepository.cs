﻿using ASPNetCourse.Core.Entities.AreaLesson;

namespace ASPNetCourse.Core.Contracts.AreaLesson
{
    public interface ICategoryLessonRepository : IRepository<CategoryLesson>
    {
        
    }
}
