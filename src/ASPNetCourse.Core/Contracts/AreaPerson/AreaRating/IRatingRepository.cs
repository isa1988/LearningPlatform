﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaRating;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaRating
{
    /// <summary>
    /// Оценка
    /// </summary>
    public interface IRatingRepository : IRepository<Rating, Guid>
    {
    }
}
