﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson;

namespace ASPNetCourse.Core.Contracts.AreaPerson
{
    /// <summary>
    /// Профиль пользователя
    /// </summary>
    public interface IPersonRepository : IRepository<Person, Guid>
    {
    }

}
