﻿
using ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo.AreaTag;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLessonInfo.AreaTag
{
    public interface ITagRepository : IRepository<Tag>
    {
    }
}
