﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLessonInfo
{
    /// <summary>
    /// Описание бронированного урока
    /// </summary>
    public interface ILessonInfoRepository : IRepository<LessonInfo, Guid>
    {

    }
}
