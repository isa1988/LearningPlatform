﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLessonInfo
{
    /// <summary>
    /// Цены за урок
    /// </summary>
    public interface IPriceOfLessonRepository : IRepository<PriceOfLesson, Guid>
    {
        
    }
}
