﻿using ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLanguage
{
    /// <summary>
    /// Родные языки для пользователя
    /// </summary>
    public interface INativeOfLanguageRepository : IRepository<NativeOfLanguage>
    {
    }
}
