﻿
using ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLanguage
{
    /// <summary>
    /// Иностранные языки пользователя 
    /// </summary>
    public interface IForeignOfLanguageRepository : IRepository<ForeignOfLanguage>
    {
    }
}
