﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson;

namespace ASPNetCourse.Core.Contracts.AreaPerson
{
    /// <summary>
    /// Контактные данные
    /// </summary>
    public interface IContactRepository : IRepository<Contact, Guid>
    {
        
    }
}
