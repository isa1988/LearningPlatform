﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaEducationalMaterial;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaEducationalMaterial
{
    /// <summary>
    /// Учебные материалы
    /// </summary>
    public interface IEducationalMaterialRepository : IRepository<EducationalMaterial, Guid>
    {
    }
}
