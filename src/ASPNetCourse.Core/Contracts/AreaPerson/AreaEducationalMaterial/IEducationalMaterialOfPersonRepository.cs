﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaEducationalMaterial;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaEducationalMaterial
{
    /// <summary>
    /// Учебные материалы пользователя
    /// </summary>
    public interface IEducationalMaterialOfPersonRepository : IRepository<EducationalMaterialOfPerson>
    {
    }
}
