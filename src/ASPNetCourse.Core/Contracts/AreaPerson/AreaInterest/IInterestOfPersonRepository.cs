﻿using ASPNetCourse.Core.Entities.AreaPerson.AreaInterest;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaInterest
{
    /// <summary>
    /// Интересы пользователя
    /// </summary>
    public interface IInterestOfPersonRepository : IRepository<InterestOfPerson>
    {
    }
}
