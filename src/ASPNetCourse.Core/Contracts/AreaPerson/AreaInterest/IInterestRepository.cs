﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaInterest;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaInterest
{
    /// <summary>
    /// Интересы
    /// </summary>
    public interface IInterestRepository : IRepository<Interest, Guid>
    {
    }
}
