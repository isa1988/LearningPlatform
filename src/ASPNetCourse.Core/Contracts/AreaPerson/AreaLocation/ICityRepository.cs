﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLocation;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLocation
{
    /// <summary>
    /// Города
    /// </summary>
    public interface ICityRepository : IRepository<City, Guid>
    {

    }
}
