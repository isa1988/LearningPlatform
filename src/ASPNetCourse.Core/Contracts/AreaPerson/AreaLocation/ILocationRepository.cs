﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLocation;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLocation
{
    /// <summary>
    /// Локация
    /// </summary>
    public interface ILocationRepository : IRepository<Location, Guid>
    {
    }
}
