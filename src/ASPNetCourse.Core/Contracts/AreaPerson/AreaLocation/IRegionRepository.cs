﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLocation;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLocation
{
    /// <summary>
    /// Регионы (области)
    /// </summary>
    public interface IRegionRepository : IRepository<Region, Guid>
    {
        
    }
}
