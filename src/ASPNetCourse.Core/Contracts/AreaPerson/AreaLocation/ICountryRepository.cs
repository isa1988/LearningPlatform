﻿using System;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLocation;

namespace ASPNetCourse.Core.Contracts.AreaPerson.AreaLocation
{
    /// <summary>
    /// Страна
    /// </summary>
    public interface ICountryRepository : IRepository<Country, Guid>
    {
    }
}
