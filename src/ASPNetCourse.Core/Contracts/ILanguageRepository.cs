﻿using System;
using ASPNetCourse.Core.Entities;

namespace ASPNetCourse.Core.Contracts
{
    public interface ILanguageRepository : IRepository<Language, Guid>
    {
    }
}
