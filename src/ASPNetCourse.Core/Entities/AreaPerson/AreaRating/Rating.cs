﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaRating
{
    /// <summary>
    /// Оценка
    /// </summary>
    public class Rating : IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        /// Оценка
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }
    }
}
