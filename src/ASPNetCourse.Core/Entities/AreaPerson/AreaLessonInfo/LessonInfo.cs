﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Entities.AreaLesson;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo.AreaTag;
using ASPNetCourse.Core.Helper.Entities;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo
{
    /// <summary>
    /// Описание бронированного урока
    /// </summary>
    public class LessonInfo : IEntity<Guid>
    {
        public LessonInfo()
        {
            PriceOfLessons = new List<PriceOfLesson>();
            TagOfLessonInfos = new List<TagOfLessonInfo>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Категория
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Категория
        /// </summary>
        public CategoryLesson CategoryLesson { get; set; }

        /// <summary>
        /// Категория
        /// </summary>
        public Guid CategoryLessonId { get; set; }

        /// <summary>
        /// Начальный уровень 
        /// </summary>
        public LevelOfLanguage FromLevel { get; set; }

        /// <summary>
        /// Конечный уровень 
        /// </summary>
        public LevelOfLanguage ToLevel { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Comment { get; set; }
        public List<PriceOfLesson> PriceOfLessons { get; set; }
        public List<TagOfLessonInfo> TagOfLessonInfos { get; set; }
    }
}
