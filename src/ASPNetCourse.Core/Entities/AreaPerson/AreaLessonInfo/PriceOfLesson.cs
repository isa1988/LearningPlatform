﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo
{
    /// <summary>
    /// Цены за урок
    /// </summary>
    public class PriceOfLesson : IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Занятие
        /// </summary>
        public LessonInfo Lesson { get; set; }

        /// <summary>
        /// Занятие
        /// </summary>
        public Guid LessonId { get; set; }

        /// <summary>
        /// Количество минут
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public float Price { get; set; }
    }
}
