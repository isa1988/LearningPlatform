﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo.AreaTag
{
    public class Tag : IEntity<Guid>
    {
        public Tag()
        {
            TagOfLessonInfos = new List<TagOfLessonInfo>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<TagOfLessonInfo> TagOfLessonInfos { get; set; }
    }
}
