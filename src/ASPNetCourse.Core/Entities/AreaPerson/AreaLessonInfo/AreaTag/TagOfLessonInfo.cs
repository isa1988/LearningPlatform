﻿using System;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo.AreaTag
{
    public class TagOfLessonInfo : IEntity
    {
        public LessonInfo LessonInfo { get; set; }
        public Guid LessonInfoId { get; set; }
        public Tag Tag { get; set; }
        public Guid TagId { get; set; }
    }
}
