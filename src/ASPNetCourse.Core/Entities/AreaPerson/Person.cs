﻿
using System;
using System.Collections.Generic;
using ASPNetCourse.Core.Entities.Accaunt;
using ASPNetCourse.Core.Entities.AreaPerson.AreaInterest;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLocation;
using ASPNetCourse.Core.Entities.AreaPerson.AreaRating;
using ASPNetCourse.Core.Entities.AreaPerson.AreaEducationalMaterial;

namespace ASPNetCourse.Core.Entities.AreaPerson
{
    /// <summary>
    /// Профиль пользователя
    /// </summary>
    public class Person : IEntity<Guid>
    {
        public Person()
        {
            NativeOfLanguages = new List<NativeOfLanguage>();
            ForeignOfLanguages = new List<ForeignOfLanguage>();
            InterestOfPersons = new List<InterestOfPerson>();
            EducationalMaterialOfPersons = new List<EducationalMaterialOfPerson>();
            Ratings = new List<Rating>();
            Locations = new List<Location>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// О себе 
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Я как преподаватель
        /// </summary>
        public string TeacherComment { get; set; }

        /// <summary>
        /// Мои уроки и стиль преподавания
        /// </summary>
        public string MyStyle { get; set; }

        /// <summary>
        /// Средний балл
        /// </summary>
        public float Rating { get; set; }

        public User User { get; set; }
        public Guid UserId { get; set; }

        public List<NativeOfLanguage> NativeOfLanguages { get; set; }
        public List<ForeignOfLanguage> ForeignOfLanguages { get; set; }
        public List<InterestOfPerson> InterestOfPersons { get; set; }
        public List<EducationalMaterialOfPerson> EducationalMaterialOfPersons { get; set; }
        public List<Rating> Ratings { get; set; }
        public List<Location> Locations { get; set; }
    }

}
