﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Helper.Entities;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage
{
    /// <summary>
    /// Иностранные языки пользователя 
    /// </summary>
    public class ForeignOfLanguage : IEntity
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        /// Язык
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Язык
        /// </summary>
        public Guid LanguageId { get; set; }

        /// <summary>
        /// Уровень
        /// </summary>
        public LevelOfLanguage Level { get; set; }
    }
}
