﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage
{
    /// <summary>
    /// Родные языки для пользователя
    /// </summary>
    public class NativeOfLanguage : IEntity
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        /// Язык
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Язык
        /// </summary>
        public Guid LanguageId { get; set; }
    }
}
