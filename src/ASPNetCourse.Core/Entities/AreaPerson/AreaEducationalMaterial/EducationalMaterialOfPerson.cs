﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaEducationalMaterial
{
    /// <summary>
    /// Учебные материалы пользователя
    /// </summary>
    public class EducationalMaterialOfPerson : IEntity
    {
        /// <summary>
        /// Учебный материал
        /// </summary>
        public EducationalMaterial EducationalMaterial { get; set; }

        /// <summary>
        /// Учебный материал
        /// </summary>
        public Guid EducationalMaterialId { get; set; }

        /// <summary>
        /// Пользоатель
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }
    }
}
