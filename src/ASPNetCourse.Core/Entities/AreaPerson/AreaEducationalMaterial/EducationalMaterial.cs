﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaEducationalMaterial
{
    /// <summary>
    /// Учебные материалы
    /// </summary>
    public class EducationalMaterial : IEntity<Guid>
    {
        public EducationalMaterial()
        {
            EducationalMaterialOfPersons = new List<EducationalMaterialOfPerson>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Учебные материалы пользователя
        /// </summary>
        public List<EducationalMaterialOfPerson> EducationalMaterialOfPersons { get; set; }
    }
}
