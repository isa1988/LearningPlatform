﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLocation
{
    /// <summary>
    /// Локация
    /// </summary>
    public class Location : IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public Guid CountryId { get; set; }

        /// <summary>
        /// Регион (область)
        /// </summary>
        public Region Region { get; set; }

        /// <summary>
        /// Регион (область)
        /// </summary>
        public Guid RegionId { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public City City { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public Guid CityId { get; set; }

        /// <summary>
        /// Отсюда \ живет 
        /// </summary>
        public bool IsBornHere { get; set; }
    }
}
