﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLocation
{
    /// <summary>
    /// Города
    /// </summary>
    public class City : IEntity<Guid>
    {
        public City()
        {
            Locations = new List<Location>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Регион (Область)
        /// </summary>
        public Region Region { get; set; }

        /// <summary>
        /// Регион (Область)
        /// </summary>
        public Guid RegionId { get; set; }

        public List<Location> Locations { get; set; }
    }
}
