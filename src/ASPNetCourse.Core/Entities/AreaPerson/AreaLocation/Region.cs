﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Helper.Entities;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLocation
{
    /// <summary>
    /// Регионы (области)
    /// </summary>
    public class Region : IEntity<Guid>
    {
        public Region()
        {
            Locations = new List<Location>();
            Cities = new List<City>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public Guid CountryId { get; set; }

        /// <summary>
        /// Часовой пояс
        /// </summary>
        public Timezone Timezone { get; set; }

        /// <summary>
        /// Города
        /// </summary>
        public List<City> Cities { get; set; }


        public List<Location> Locations { get; set; }
    }
}
