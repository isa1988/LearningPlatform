﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaLocation
{
    /// <summary>
    /// Страна
    /// </summary>
    public class Country : IEntity<Guid>
    {
        public Country()
        {
            Regions = new List<Region>();
            Locations = new List<Location>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Регионы
        /// </summary>
        public List<Region> Regions { get; set; }
        public List<Location> Locations { get; set; }
    }
}
