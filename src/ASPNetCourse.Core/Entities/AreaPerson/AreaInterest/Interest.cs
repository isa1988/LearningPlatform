﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaInterest
{
    /// <summary>
    /// Интересы
    /// </summary>
    public class Interest : IEntity<Guid>
    {
        public Interest()
        {
            InterestOfPersons = new List<InterestOfPerson>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        
        public Guid Id { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Интересы пользователя
        /// </summary>
        public List<InterestOfPerson> InterestOfPersons { get; set; }
    }
}
