﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPNetCourse.Core.Entities.AreaPerson.AreaInterest
{
    /// <summary>
    /// Интересы пользователя
    /// </summary>
    public class InterestOfPerson : IEntity
    {
        /// <summary>
        /// Интерес
        /// </summary>
        public Interest Interest { get; set; }

        /// <summary>
        /// Интерес
        /// </summary>
        public Guid InterestId { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Guid PersonId { get; set; }
    }
}
