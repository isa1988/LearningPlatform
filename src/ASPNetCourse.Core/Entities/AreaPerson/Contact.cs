﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Helper.Entities;

namespace ASPNetCourse.Core.Entities.AreaPerson
{
    /// <summary>
    /// Контактные данные
    /// </summary>
    public class Contact : IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Пользхователь
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Поьзователь
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        /// Тип контакта
        /// </summary>
        public ContactType ContactType { get; set; }
    }
}
