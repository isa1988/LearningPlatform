﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLessonInfo;

namespace ASPNetCourse.Core.Entities.AreaLesson
{
    public class CategoryLesson : IEntity<Guid>
    {
        public CategoryLesson()
        {
            LessonInfos = new List<LessonInfo>();
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Уроки описание
        /// </summary>
        public List<LessonInfo> LessonInfos { get; set; }
    }
}
