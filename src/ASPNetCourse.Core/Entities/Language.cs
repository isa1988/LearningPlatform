﻿using System;
using System.Collections.Generic;
using System.Text;
using ASPNetCourse.Core.Entities.AreaPerson.AreaLanguage;

namespace ASPNetCourse.Core.Entities
{
    /// <summary>
    /// Язык
    /// </summary>
    public class Language : IEntity<Guid>
    {
        public Language()
        {
            NativeOfLanguages = new List<NativeOfLanguage>();
            ForeignOfLanguages = new List<ForeignOfLanguage>();
        }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Родные для пользователя
        /// </summary>
        public List<NativeOfLanguage> NativeOfLanguages { get; set; }

        /// <summary>
        /// Иностранные для пользователя
        /// </summary>
        public List<ForeignOfLanguage> ForeignOfLanguages { get; set; }
    }
}
