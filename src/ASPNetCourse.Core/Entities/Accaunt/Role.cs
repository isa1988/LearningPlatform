﻿using Microsoft.AspNetCore.Identity;

namespace ASPNetCourse.Core.Entities.Accaunt
{
    /// <summary>
    /// Роль
    /// </summary>
    public class Role : IdentityRole<int>
    {
        public Role(string name) : base(name)
        {
        }
    }
}
