﻿using Microsoft.AspNetCore.Identity;

namespace ASPNetCourse.Core.Entities.Accaunt
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : IdentityUser<int>
    {
    }
}
